
#include <fem.hpp>
//#include <comp.hpp>
#include <python_ngstd.hpp>
#include "myCoefficient.hpp"

namespace ngfem
{
  // Export cf to Python
 
  void ExportReferenceSolution(py::module m)
  {
    py::class_<ReferenceSolution, shared_ptr<ReferenceSolution>, CoefficientFunction>
      (m, "ReferenceSolution", "Referene solution for scattering problem")
      .def(py::init<>())
      ;
  }
  
  void ExportPlaneWaveHomSolution(py::module m)
  {

    m.def("PlaneWaveHomSolution", [](double aomega, double aa, std::string abnd_type = "sound-soft") -> shared_ptr<CoefficientFunction>
    {
      auto PWHomSol = shared_ptr<CoefficientFunction>(make_shared<PlaneWaveHomSolution> (aomega,aa,abnd_type));
      return PWHomSol;
    });

  }




  void ExportKiteSolution(py::module m)
  {
     
     /* py::class_<KiteSolution, shared_ptr<KiteSolution>, CoefficientFunction>
      (m, "KiteSolution", "Reference solution for scattering by kite shaped domain")
      .def(py::init<>())
      ; */ 
   
    m.def("KiteSolution", [](double akappa, int an,bool ause_deriv) -> shared_ptr<CoefficientFunction>
    {
      auto KiteSol = shared_ptr<CoefficientFunction>(make_shared<KiteSolution> (akappa,an,ause_deriv));
      return KiteSol;
    });

  }


  // KiteSolution  

  KiteSolution :: KiteSolution(double akappa,int an,bool ause_deriv)  : CoefficientFunction(/*dimension = */ 1,true) { 
    
    kappa = akappa; 
    n = an; 
    use_deriv = ause_deriv; 
    eta = kappa; 
    Vector<double> direc = {1.0,0.0}; 


    /* Solve boundary integral equation using Nystroem method */ 

    // right hand side for linear system;

    // Vector<double> ts(2*n);
    for(int i= 0; i < 2*n; i++) {
      ts.push_back(M_PI*i/n);
    }

    Matrix<Complex> A(2*n,2*n);
    for(int i=0;i<2*n;i++){
      for(int j=0;j<2*n;j++){
	A(i,j) = 0.0;
        if (i==j) {
	  A(i,j) += 1;
	}
	A(i,j) -= ( RR(0,ts,int(abs(i-j)),n)*K1(kappa,eta,ts[i],ts[j]) + (M_PI/n)*K2(kappa,eta,ts[i],ts[j]) );
      }
    }

    Vector<Complex> b(2*n);
    for(int i=0; i<2*n;i++) {
      b(i) =  -2*exp(Complex(0,1)*kappa*InnerProduct(direc, X(ts[i])));
    }
    CalcInverse(A);
    auto tmp = A*b;
    for(int i= 0; i<2*n;i++){
      psi.push_back(tmp(i));
    }

    
    // Calculate far field:
    Complex gamma = (M_PI/n)*exp(-Complex(0,1)*M_PI*0.25)/sqrt(8*M_PI*kappa);
    Complex result = 0.0;
    for(int j=0; j<2*n;j++) {
      Vector <Complex> normal = { dX(ts[j])(1)/dX_r(ts[j]) ,-dX(ts[j])(0)/dX_r(ts[j])   };
      result += (kappa*InnerProduct(normal,direc)+eta)*exp(-Complex(0,1)*kappa*InnerProduct(direc,X(ts[j])))*psi[j]*dX_r(ts[j]);
    }
    result *= gamma; 
    cout << "far field  = " << std::setprecision(9) << result  << endl;

  }

	

  void ExportFundamentalSolution(py::module m)
  {
     
    m.def("FundamentalSolution", [](double aomega, double ax_source, double ay_source,bool ause_deriv) -> shared_ptr<CoefficientFunction>
    {
      auto FundamentalSol = shared_ptr<CoefficientFunction>(make_shared<FundamentalSolution> (aomega,ax_source,ay_source,ause_deriv));
      return FundamentalSol;
    });

  }
  
  void ExportFundamentalSolution_grad(py::module m)
  {
     
    m.def("FundamentalSolution_grad", [](double aomega, double ax_source, double ay_source,bool a_ydir) -> shared_ptr<CoefficientFunction>
    {
      auto FundamentalSol_grad = shared_ptr<CoefficientFunction>(make_shared<FundamentalSolution_grad> (aomega,ax_source,ay_source,a_ydir));
      return FundamentalSol_grad;
    });

  }
 
  void ExportFundamentalSolution_dxx(py::module m)
  {
     
    m.def("FundamentalSolution_dxx", [](double aomega, double ax_source, double ay_source) -> shared_ptr<CoefficientFunction>
    {
      auto FundamentalSol_dxx = shared_ptr<CoefficientFunction>(make_shared<FundamentalSolution_dxx> (aomega,ax_source,ay_source));
      return FundamentalSol_dxx;
    });

  }

  void ExportFundamentalSolution_dyy(py::module m)
  {
     
    m.def("FundamentalSolution_dyy", [](double aomega, double ax_source, double ay_source) -> shared_ptr<CoefficientFunction>
    {
      auto FundamentalSol_dyy = shared_ptr<CoefficientFunction>(make_shared<FundamentalSolution_dyy> (aomega,ax_source,ay_source));
      return FundamentalSol_dyy;
    });

  }

  void ExportFundamentalSolution_dxy(py::module m)
  {
     
    m.def("FundamentalSolution_dxy", [](double aomega, double ax_source, double ay_source) -> shared_ptr<CoefficientFunction>
    {
      auto FundamentalSol_dxy = shared_ptr<CoefficientFunction>(make_shared<FundamentalSolution_dxy> (aomega,ax_source,ay_source));
      return FundamentalSol_dxy;
    });

  }


  void ExportEllipticFundamentalSolution(py::module m)
  {
     
    m.def("EllipticFundamentalSolution", [](double aomega, double ac, double amu_source, double anu_source,bool ause_deriv) -> shared_ptr<CoefficientFunction>
    {
      auto EllipticFundamentalSol = shared_ptr<CoefficientFunction>(make_shared<EllipticFundamentalSolution> (aomega,ac,amu_source,anu_source,ause_deriv));
      return EllipticFundamentalSol;
    });

  }

  void ExportRealSepSolution(py::module m)
  {
     
    m.def("RealSepSolution", [](double aomega, int aL) -> shared_ptr<CoefficientFunction>
    {
      auto RealSepSol = shared_ptr<CoefficientFunction>(make_shared<RealSepSolution> (aomega,aL));
      return RealSepSol;
    });

  }

  void ExportJumpSolution(py::module m)
  {

    m.def("JumpSolution", [](double aomega_I,double aomega_inf,double aR_S, double aR_J) -> shared_ptr<CoefficientFunction>
    {
      auto JumpSol = shared_ptr<CoefficientFunction>(make_shared<JumpSolution> (aomega_I,aomega_inf,aR_S,aR_J));
      return JumpSol;
    });

  }


  void ExportJumpSolutionIncomingWave(py::module m)
  {

    m.def("JumpSolutionIncomingWave", [](double aomega_I,double aomega_inf,double aR_S, double aR_J,double aomega_inc) -> shared_ptr<CoefficientFunction>
    {
      auto JumpSol = shared_ptr<CoefficientFunction>(make_shared<JumpSolution> (aomega_I,aomega_inf,aR_S,aR_J,aomega_inc));
      return JumpSol;
    });

  }


  JumpSolution :: JumpSolution(double aomega_I,double aomega_inf, double aR_S, double aR_J) : CoefficientFunction(/*dimension = */ 1,true) { 
      omega_I = aomega_I;
      omega_inf = aomega_inf;
      R_S = aR_S;        
      R_J = aR_J; 
    
      for(int l= 0; l < Lmax; l++) {
        Al.push_back(get_A(l));
        Bl.push_back(get_B(l));
	if( l == 0) {
          data_coeff.push_back(cyl_bessel_j(l,omega_I*R_S)*vol_Gamma); 
	}
	else {
          data_coeff.push_back(2*pow(Complex(0,1),l)*cyl_bessel_j(l,omega_I*R_S)*sqrt(M_PI*R_S)); 
	}
      }

  }
  
   JumpSolution :: JumpSolution(double aomega_I,double aomega_inf, double aR_S, double aR_J,double omega_inc) : CoefficientFunction(/*dimension = */ 1,true) { 
      omega_I = aomega_I;
      omega_inf = aomega_inf;
      R_S = aR_S;        
      R_J = aR_J; 
    
      for(int l= 0; l < Lmax; l++) {
        Al.push_back(get_A(l));
        Bl.push_back(get_B(l));
	if( l == 0) {
          data_coeff.push_back(cyl_bessel_j(l,omega_inc*R_S)*vol_Gamma); 
	}
	else {
          data_coeff.push_back(2*pow(Complex(0,1),l)*cyl_bessel_j(l,omega_inc*R_S)*sqrt(M_PI*R_S)); 
	}
      }

  }


  // Register cf for pickling/archiving
  // Create static object with template parameter function and base class.
  // static RegisterClassForArchive<ReferenceSolution, CoefficientFunction> regRefSol;
  // static RegisterClassForArchive<PlaneWaveHomSolution, CoefficientFunction> regPWHomSol;
  // static RegisterClassForArchive<KiteSolution, CoefficientFunction> regKiteSol;
  // static RegisterClassForArchive<FundamentalSolution, CoefficientFunction> regFundSol;
  // static RegisterClassForArchive<RealSepSolution, CoefficientFunction> regRealSepSol;
  // static RegisterClassForArchive<JumpSolution, CoefficientFunction> regJumpSol;

}
